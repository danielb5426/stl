//
//  Customer.hpp
//  lesson_10
//
//  Created by daniel benassaya on 08/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//

#ifndef Customer_hpp
#define Customer_hpp

#include <stdio.h>
#include"Item.hpp"
#include<set>

class Customer
{
public:
    Customer(std::string name);
    ~Customer();
    double totalSum() const;//returns the total sum for payment
    void addItem(Item item);//add item to the set
    void removeItem(Item);//remove item from the set
   

    //get and set functions
    std::string getName();
    std::set<Item>& getItems();
private:
    
    std::string _name;
    std::set<Item> _items;


};

#endif /* Customer_hpp */

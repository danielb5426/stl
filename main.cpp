//
//  main.cpp
//  lesson_10
//
//  Created by daniel benassaya on 08/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//

#include <iostream>
#include"Customer.hpp"
#include<map>
#include <stdexcept>

void buyItem(std::map<std::string, Customer>& abcCustomers, std::string name, Item itemList[10]);
void printCustomerItem(std::map<std::string, Customer> abcCustomers, std::string name);

int main()
{
    Item itemList[10] = {
    Item("Milk","00001",5.3),
    Item("Cookies","00002",12.6),
    Item("bread","00003",8.9),
    Item("chocolate","00004",7.0),
    Item("cheese","00005",15.3),
    Item("rice","00006",6.2),
    Item("fish", "00008", 31.65),
    Item("chicken","00007",25.99),
    Item("cucumber","00009",1.21),
    Item("tomato","00010",2.32)};
    
    int choise = 0, choise2 = 0, choise3 = 1;
    std::string name, itemName;
    std::map<std::string, Customer> abcCustomers;

    while (choise != 4)
    {
        std::cout << "Welcome to MagshiMart!\n1. to sign as customer and buy items\n2. to uptade existing customer's items\n3. to print the customer who pays the most\n4. to exit\n";
        std::cin >> choise;
        
        switch (choise)
        {
            case 1:
            {
                std::cout << "Please enter your name: ";
                std::cin >> name;
                if (abcCustomers.find(name) == abcCustomers.end())// check if name alredy exist
                {
                    abcCustomers.insert(std::pair<std::string, Customer>(name, Customer(name)));
                    buyItem(abcCustomers, name, itemList);
                }
                else
                {
                    std::cerr << "This name alredy exist!\n";
                }
                break;
            }
            case 2:
            {
                std::cout << "Please enter your name: ";
                std::cin >> name;
                if (abcCustomers.find(name) != abcCustomers.end())// check if name alredy exist
                {
                    printCustomerItem(abcCustomers, name);
                    std::cout << "\n1. Add items\n2. Remove items\n3. Back to menu\n";
                    std::cin >> choise2;
                    if (choise2 == 1)
                    {
                        buyItem(abcCustomers, name, itemList);
                    }
                    else if (choise2 == 2)
                    {
                        auto beginIt = abcCustomers.find(name)->second.getItems().begin();
                        choise3 = 1;
                        while (choise3)
                        {
                            printCustomerItem(abcCustomers, name);
                            std::cout << "Wich item do you want to erase(0 to exit)?\n";
                            std::cin >> choise3;
                            if (choise3 > abcCustomers.find(name)->second.getItems().size() || choise3 < 0)
                            {
                                std::cerr << "wrong choise!";
                                exit(1);
                            }
                            if (choise3)
                            {
                                beginIt = abcCustomers.find(name)->second.getItems().begin().operator++(choise3 - 1);
                            }
                            if (choise3 && beginIt->getCount() == 1)
                            {
                                abcCustomers.find(name)->second.getItems().erase(*beginIt);
                            }
                            else if(choise3)
                            {
                                Item a("", "", 0);
                                a.setName(beginIt->getName());
                                a.setSerialNumber(beginIt->getSerialNumber());
                                a.setUnitPrice(beginIt->getUnitPrice());
                                a.setCount(beginIt->getCount() - 1);
                                abcCustomers.find(name)->second.getItems().erase(*beginIt);
                                abcCustomers.find(name)->second.getItems().insert(a);

                            }
                        }
                    }
                }
                break;
            }
            case 3:
            {
                if (abcCustomers.begin() == abcCustomers.end())
                {
                    std::cout << "we dont have customers\n";
                    break;
                }
                std::map<std::string, Customer>::iterator maxIt;
                int max = 0;
                for (auto i = abcCustomers.begin(); i != abcCustomers.end(); ++i)
                {
                    if (i->second.totalSum() > max)
                    {
                        maxIt = i;
                        max = i->second.totalSum();
                    }
                }
                std::cout << "richest man in the merket:\nname: " << maxIt->second.getName() << "\ntotal sum he pay: " << maxIt->second.totalSum() << "\n" <<  " item he buy:" << std::endl;
                printCustomerItem(abcCustomers, maxIt->second.getName());
                break;
            }
            default:
            {
                break;
            }
        }
    }

    return 0;
}

void printCustomerItem(std::map<std::string, Customer> abcCustomers, std::string name)
{
    auto itEnd = abcCustomers.find(name)->second.getItems().end();
    auto it = abcCustomers.find(name)->second.getItems().begin();
    for (int a = 1; it != itEnd; ++it, a++)//print user's item
    {
        std::cout << a << " - " << it->getName() << std::endl;
    }
}

void buyItem(std::map<std::string, Customer>& abcCustomers, std::string name, Item itemList[10])
{
    
    int itemChoise = 1;
    while (itemChoise)
    {
        std::cout << "The items you can buy are: (0 to exit)\n";
        for (int i = 0; i < 10; i++)
        {
            std::cout << i + 1 << " - " << itemList[i].getName() << " price: " << itemList[i].totalPrice() << std::endl;
        }
        std::cin >> itemChoise;
        if (itemChoise > 10 || itemChoise < 0)
        {
            std::cerr << "invalide choise\n";
            return;
        }
        if (itemChoise)
        {
            abcCustomers.find(name)->second.addItem(itemList[itemChoise - 1]);
        }
    }
}

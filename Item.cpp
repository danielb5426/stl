//
//  Item.cpp
//  lesson_10
//
//  Created by daniel benassaya on 08/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//

#include "Item.hpp"


Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
    _name = name;
    _serialNumber = serialNumber;
    _count = 1;
    _unitPrice = unitPrice;
}

std::string Item::getSerialNumber() const
{
    return _serialNumber;
}

double Item::getUnitPrice() const
{
    return _unitPrice;
}

void Item::setName(std::string name)
{
    _name = name;
}
void Item::setSerialNumber(std::string serialNumber)
{
    _serialNumber = serialNumber;
}
void Item::setUnitPrice(double unitPrice)
{
    _unitPrice = unitPrice;
}

Item::~Item()
{
}

std::string Item::getName() const
{
    return _name;
}

double Item::totalPrice() const
{
    return _count * _unitPrice;
}

bool Item::operator <(const Item& other) const
{
    return _serialNumber < other._serialNumber;
}

bool Item::operator >(const Item& other) const
{
    return _serialNumber > other._serialNumber;
}

bool Item::operator==(const Item& other) const
{
    return _serialNumber == other._serialNumber;
}

void Item::setCount(int count)
{
    _count = count;
}

int Item::getCount() const
{
    return _count;
}

//
//  Item.hpp
//  lesson_10
//
//  Created by daniel benassaya on 08/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//

#ifndef Item_hpp
#define Item_hpp

#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
    
    Item(std::string name, std::string serialNumber, double unitPrice);
    ~Item();
    
    double totalPrice() const; //returns _count*_unitPrice
    bool operator <(const Item& other) const; //compares the _serialNumber of those items.
    bool operator >(const Item& other) const; //compares the _serialNumber of those items.
    bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

    //get
    std::string getName() const;
    int getCount() const;
    std::string getSerialNumber() const;
    double getUnitPrice() const;
    //set
    void setName(std::string name);
    void setSerialNumber(std::string serialNumber);
    void setUnitPrice(double unitPrice);
    void setCount(int count);

private:
    
    std::string _name;
    std::string _serialNumber; //consists of 5 numbers
    int _count; //default is 1, can never be less than 1!
    double _unitPrice; //always bigger than 0!
};

#endif /* Item_hpp */

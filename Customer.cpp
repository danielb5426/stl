//
//  Customer.cpp
//  lesson_10
//
//  Created by daniel benassaya on 08/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//

#include "Customer.hpp"

Customer::Customer(std::string name)
{
    _name = name;
}
Customer::~Customer()
{
}

double Customer::totalSum() const
{
    double sum = 0;
    std::set<Item>::iterator it = _items.begin();
    for (; it != _items.end(); ++it)
    {
        sum += it.operator*().totalPrice();
    }
    return sum;
}

void Customer::addItem(Item item)
{
    std::set<Item>::iterator x = _items.find(item);
    if ( x == _items.end())//check if item alredy exist
    {
        _items.insert(item);
    }
    else
    {
        Item a("", "", 0);
        a.setName(x->getName());
        a.setSerialNumber(x->getSerialNumber());
        a.setUnitPrice(x->getUnitPrice());
        a.setCount(x->getCount() + 1);
        _items.erase(item);
        _items.insert(a);
    }
}

void Customer::removeItem(Item item)
{
    _items.erase(item);
}

std::set<Item>& Customer::getItems()
{
    return _items;
}

std::string Customer::getName()
{
    return _name;
}
